/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Antoniu
 */
class Admitere {

    List<Student> students = new ArrayList<>();
    
    void AddStudent(Student student) {
        students.add(student);
    }
    
    void ShowAcceptedStudents(int noOfStudents) {
        
        Collections.sort(students, (s1, s2) -> {
            return s2.grade - s1.grade;
        });
        
        students
                .subList(0, noOfStudents)
                .forEach((s) -> s.ConsoleShow());
    }
    
}
