
package javaapplication4;


public class JavaApplication4 {


    public static void main(String[] args) {
         Admitere admitere = new Admitere();
         
         admitere.AddStudent(new Student("Toni",5));
         admitere.AddStudent(new Student("Dorel",7));
         admitere.AddStudent(new Student("Nicu",8));
         admitere.AddStudent(new Student("Zorel",6));
         
         admitere.ShowAcceptedStudents(2);
    }
    
}
